# Pairing
Hold the home button for 10-15 seconds.
[log](data/amazon_remote_pairing.log)

# Buttons
This remote sends payload of variable size - either 5 or 6 bytes.
[dump](data/amazon_remote.bts)

|    **Name**      | **Value**  |
|------------------|------------| 
| Power On/Off     | 0x660000   |
| Circular middle  | 0x580000   |
| Circular up      | 0x520000   |
| Circular down    | 0x510000   |
| Circular left    | 0x500000   |
| Circular right   | 0x4f0000   |
| Back             | 0xf10000   |
| Alexa            | 0x21020000 |
| Home             | 0x23020000 |
| Menu             | 0x40000000 |
| Fast Back        | 0xb4000000 |
| Play/Stop        | 0xcd000000 |
| Fast Forward     | 0xb3000000 |
| Mute             | 0xe2000000 |
| Television       | 0x8d000000 |
| Volume up        | 0xe9000000 |
| Volume down      | 0xea000000 |
| Prime Video      | 0xa1000000 |
| Netflix          | 0xa2000000 |
| Disney Plus      | 0xa3000000 |
| Amazon Music     | 0xa4000000 |
