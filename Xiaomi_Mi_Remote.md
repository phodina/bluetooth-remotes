# Pairing
Hold the Layout and Circle buttons at the same time.
[log](data/xiaomi_remote_pairing.log)

# Buttons
This remote sends payload of same size - 5 bytes.
[dump](data/xiaomi_mi_remote.bts)

|   **Name**    | **Value** |
|---------------|-----------|
| Power On/Off  |  0x200000 |
| Circle middle |  0x010000 |
| Circle up     |  0x020000 |
| Circle down   |  0x040000 |
| Circle left   |  0x080000 |
| Circle right  |  0x100000 |
| Layout        |  0x000200 |
| Back          |  0x000008 |
| Circle        |  0x000004 |
| Netflix       |  0x000400 |
| Prime Video   |  0x002000 |
| Volume Up     |  0x800000 |
| Volume Down   |  0x000100 |

