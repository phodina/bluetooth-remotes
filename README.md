# Bluetooth Controllers for Android TV/Boxes/Sticks

The purpose of this repository is to dump information about various bluetooth
remote controllers that come with the "smart" Android TV/boxes/sticks.

## Pairing the remote
Each remote has it's own paring procedure, layout and number of buttons.

## Buttons
Each button has different code. The length of the payload for sending the information about the pressed key is
usually 5 bytes but Amazon also sends 6 bytes for some buttons.

The dump of the values can be found either in README for the controller or in
the data as raw values captured by btmon.

## Voice assistant
The remotes also have embedded microphone and dedicated button that starts
the voice capture.

It seems there needs to be some interaction from the Android device as the
remote just sends the infromation about the button being pressed but does
not send any another payload after.

# Linux
The buttons in Linux do not seem to be mapped to any input.

# Remotes
![Amazon Remote](images/Amazon_Remote.jpg)

[Amazon Remote](Amazon_Remote.md)

![Xiaomi Remote](images/Xiaomi_Mi_Remote.jpg)

[Xiaomi_Remote](Xiaomi_Mi_Remote.md)
